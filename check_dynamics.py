# MIT License

# Copyright (c) 2024 dechin

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import site
from pathlib import Path

site_path = Path(site.getsitepackages()[0])
site_file_path = site_path.parent.parent.parent / 'cyfes' / 'libcufes.so'
site_dynamics_path = str(site_file_path)

user_site_path = Path(site.USER_SITE)
user_file_path = user_site_path.parent.parent.parent / 'cyfes' / 'libcufes.so'
user_dynamics_path = str(user_file_path)

if not os.path.exists(site_dynamics_path) and not os.path.exists(user_dynamics_path):
    print ('Check dynamics complete, no libcufes.so file founded!')
else:
    print ('Installation of CyFES success!')

