# MIT License

# Copyright (c) 2024 dechin

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import numpy as np
np.random.seed(0)

def test_path_fes():
    from cyfes import PathFES
    atoms = 4
    cvs = 1000
    crd = np.random.random((atoms, 3))
    cv = np.random.random((cvs, 3))
    bw = np.random.random(3)
    bias = np.random.random(cvs)-1
    fes = np.asarray(PathFES(crd, cv, bw, bias))
    print (fes)

def test_path_fes_f32():
    from cyfes import PathFES_f32
    atoms = 4
    cvs = 1000
    crd = np.random.random((atoms, 3)).astype(np.float32)
    cv = np.random.random((cvs, 3)).astype(np.float32)
    bw = np.random.random(3).astype(np.float32)
    bias = (np.random.random(cvs)-1).astype(np.float32)
    fes = np.asarray(PathFES_f32(crd, cv, bw, bias))
    print (fes)

def test_fast_fes():
    from cyfes import FastPathFES
    atoms = 4
    cvs = 1000
    crd = np.random.random((atoms, 3))
    cv = np.random.random((cvs, 3))
    bw = np.random.random(3)
    bias = np.random.random(cvs)-1
    fes = np.asarray(FastPathFES(crd, cv, bw, bias))
    print (fes)

def test_stream_fes():
    from cyfes import StreamPathFES
    atoms = 4
    cvs = 1000
    crd = np.random.random((atoms, 3))
    cv = np.random.random((cvs, 3))
    bw = np.random.random(3)
    bias = np.random.random(cvs)-1
    fes = np.asarray(StreamPathFES(crd, cv, bw, bias))
    print (fes)

def test_device_fes():
    from cyfes import DevicePathFES
    atoms = 4
    cvs = 1000
    crd = np.random.random((atoms, 3))
    cv = np.random.random((cvs, 3))
    bw = np.random.random(3)
    bias = np.random.random(cvs)-1
    devices = np.array([0,], np.int32)
    fes = np.asarray(DevicePathFES(crd, cv, bw, bias, devices))
    print (fes)

if __name__ == '__main__':
    test_path_fes()
    test_path_fes_f32()
    test_fast_fes()
    test_stream_fes()
    test_device_fes()
